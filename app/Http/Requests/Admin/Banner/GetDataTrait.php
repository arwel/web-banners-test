<?php

namespace App\Http\Requests\Admin\Banner;

trait GetDataTrait
{
    public function getData()
    {
        return $this->only(array_keys($this->rules()));
    }
}
