<?php

namespace App\Http\Requests\Admin\Banner;

use App\Rules\Banner;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CreateRequest extends FormRequest
{
    use GetDataTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = Auth::user();

        return $user && $user->hasRole('admin');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|min:3|max:100|unique:banners,title',
            'desc' => 'required|string|min:3',
            'preview' => 'required|file|mimes:png,jpg,jpeg',
            'archive' => [
                'required',
                'file',
                'mimes:zip',
                'mimetypes:application/zip,application/octet-stream,application/x-zip-compressed,multipart/x-zip',
                new Banner(),
            ],
        ];
    }
}
