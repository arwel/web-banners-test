<?php

namespace App\Http\Controllers\Client;

use App\Banner;
use App\Collection;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class BannerController extends Controller
{
    public function index()
    {
        $banners = Banner::getPaginated();

        return view('client.banners.list', [
            'items' => $banners,
        ]);
    }

    public function show(string $slug)
    {
        try {
            $banner = Banner::where('slug', $slug)->firstOrFail();
            $currentUser = Auth::user();

            return view('client.banners.show', [
                'item' => $banner,
                'is_in_collection' => $currentUser && $banner->isInCollection($currentUser),
                'collection' => $currentUser ? $banner->collections()->where('user_id', $currentUser->id)->first() : null,
            ]);
        } catch (ModelNotFoundException $e) {
            flash()->error('Banner not found');

            return redirect()->route('web.client.banners.list');
        }
    }

    public function embed(string $token, $path = 'index.html')
    {
        try {
            $collection = Collection::where('token', $token)->with('banner')->firstOrFail();
            $disk = Storage::disk('private');
            $absPath = $collection->banner->archive . DIRECTORY_SEPARATOR . $path;

            return $disk->exists($absPath) ? $disk->get($absPath) : response('Not Found', 404);
        } catch (ModelNotFoundException $e) {
            return redirect()->route('web.client.home');
        }
    }
}
