<?php

namespace App\Http\Controllers\Client;

use App\Banner;
use App\Collection;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Ramsey\Uuid\Uuid;

class CollectionController extends Controller
{
    public function index()
    {
        $banners = Auth::user()->favoriteBanners()->getPaginated();

        return view('client.collections.list', [
            'items' => $banners,
        ]);
    }

    public function store(Banner $banner)
    {
        $favorite = Auth::user()->favoriteBanners();
        $favorite->detach($banner);
        $favorite->attach($banner, [
            'token' => Uuid::uuid4()->toString(),
        ]);

        flash()->success('Added to collection');

        return redirect()->route('web.client.banners.show', ['slug' => $banner->slug,]);
    }

    public function delete(Banner $banner)
    {
        Auth::user()->favoriteBanners()->detach($banner);

        flash()->success('Removed from collection');

        return redirect()->route('web.client.banners.list');
    }
}
