<?php

namespace App\Http\Controllers\Admin;

use App\Abstractions\Http\CanUploadFileTrait;
use App\Banner;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Banner\CreateRequest;
use App\Http\Requests\Admin\Banner\UpdateRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BannerController extends Controller
{
    use CanUploadFileTrait;

    public function index(Request $request)
    {
        $banners = $request->get('type') === 'trashed'
        ? Banner::onlyTrashed()->getPaginated()
        : Banner::getPaginated();

        return view('admin.banners.list', [
            'items' => $banners,
        ]);
    }

    public function create()
    {
        return view('admin.banners.form', [
            'form_action' => route('web.admin.banners.store'),
            'form_method' => 'POST',
            'item' => new Banner(),
        ]);
    }

    public function store(CreateRequest $request)
    {
        $previewUrl = $this->getUploadedFileUrl($request, 'preview', 'public');
        $archivePath = $this->getUploadedDirectory($request, 'archive', 'private');

        $data = $request->getData();
        $banner = Banner::create([
            'title' => $data['title'],
            'desc' => $data['desc'],
            'preview' => $previewUrl,
            'archive' => $archivePath,
            'user_id' => Auth::user()->id,
        ]);

        if ($banner) {
            flash()->success('Created');
        } else {
            flash()->error('Can not create');

            return redirect()->route('web.admin.banners.create')->withInput();
        }

        return redirect()->route('web.admin.banners.edit', ['banner' => $banner]);
    }

    public function edit(Banner $banner)
    {
        return view('admin.banners.form', [
            'form_action' => route('web.admin.banners.update', ['banner' => $banner]),
            'form_method' => 'PATCH',
            'item' => $banner,
        ]);
    }

    public function update(UpdateRequest $request, Banner $banner)
    {
        $previewUrl = $this->getUploadedFileUrl($request, 'preview', 'public');
        $archivePath = $this->getUploadedDirectory($request, 'archive', 'private');

        $banner->title = $request->input('title');
        $banner->desc = $request->input('desc');
        $banner->preview = !empty($previewUrl) ? $previewUrl : $banner->preview;
        $banner->archive = !empty($archivePath) ? $archivePath : $banner->archive;

        if ($banner->save()) {
            flash()->success('Created');
        } else {
            flash()->error('Can not create');
        }

        return redirect()->route('web.admin.banners.edit', ['banner' => $banner]);
    }

    public function trash(Banner $banner)
    {
        $banner->delete();

        flash()->success('Trashed');

        return redirect()->route('web.admin.banners.list');
    }

    public function restore($banner)
    {
        try {
            $banner = Banner::onlyTrashed()->findOrFail($banner);
            $banner->restore();

            flash()->success('Restored');
        } catch (ModelNotFoundException $e) {
            flash()->error('Item not found');
        } finally {
            return redirect()->route('web.admin.banners.list', ['type' => 'trashed']);
        }
    }

    public function delete($banner)
    {
        try {
            $banner = Banner::onlyTrashed()->findOrFail($banner);
            $banner->forceDelete();

            flash()->success('Permanently deleted');
        } catch (ModelNotFoundException $e) {
            flash()->error('Item not found');
        } finally {
            return redirect()->route('web.admin.banners.list', ['type' => 'trashed']);
        }
    }

    public function clearTrash()
    {
        Banner::onlyTrashed()->forceDelete();

        flash()->success('Trash cleared');

        return back();
    }
}
