<?php

namespace App;

use App\Abstractions\Eloquent\HasPaginationTrait;
use App\Collection;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class Banner extends Model
{
    use SoftDeletes;
    use HasPaginationTrait;

    protected $fillable = [
        'title',
        'slug',
        'desc',
        'preview',
        'archive',
        'user_id',
    ];

    public static function boot()
    {
        parent::boot();

        static::saving(function ($model) {
            $model->slug = \Slugify::slugify($model->title);
        });
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function collections()
    {
        return $this->hasMany(Collection::class);
    }

    public function isInCollection(User $user): bool
    {
        return (bool) $user->favoriteBanners()->where('banner_id', $this->id)->first();
    }
}
