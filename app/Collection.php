<?php

namespace App;

use App\Abstractions\Eloquent\HasPaginationTrait;
use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;

class Collection extends Model
{
    use HasPaginationTrait;

    protected $fillable = [
        'user_id',
        'token',
        'banner_id',
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->token = Uuid::uuid4()->toString();
        });
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function banner()
    {
        return $this->belongsTo(Banner::class);
    }
}
