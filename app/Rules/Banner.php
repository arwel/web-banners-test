<?php

namespace App\Rules;

use App\Exceptions\BannerValidationException;
use Chumper\Zipper\Facades\Zipper;
use Illuminate\Contracts\Validation\Rule;

class Banner implements Rule
{
    protected $msg;
    protected $zip;
    protected $uploaded;
    protected $uploadedName;

    public function __construct()
    {
        $this->msg = '';
        $this->zip = null;
        $this->uploaded = null;
        $this->uploadedName = '';
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $result = false;
        $realPath = $value->getRealPath();

        try {
            $this->uploaded = $value;
            $this->uploadedName = pathinfo($this->uploaded->getClientOriginalName(), PATHINFO_FILENAME);
            $this->zip = Zipper::make($realPath);

            $this
            ->rootDirNameAsFileAssert()
            ->containIndexHtmlAssert()
            ->containDeniedFilesAssert()
            ->assetsLoadedOverHttpsOrRelativeAssert()
            ;

            $result = true;
        } catch (BannerValidationException $e) {
            $this->msg = $e->getMessage();
            unlink($realPath);

            $result = false;
        } finally {
            return $result;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->msg;
    }

    protected function rootDirNameAsFileAssert()
    {
        if (count($this->zip->listFiles("/^{$this->uploadedName}\//isU")) < 1) {
            throw new BannerValidationException('Archive must contain directory with the same name as it');
        }

        return $this;
    }

    protected function containIndexHtmlAssert()
    {
        if (count($this->zip->listFiles("/^{$this->uploadedName}\/index\.html$/isU")) < 1) {
            throw new BannerValidationException('Banner must contain index.html file');
        }

        return $this;
    }

    protected function containDeniedFilesAssert()
    {
        $acceptedExtensions = implode('|', config('app.accepted_extensions', []));
        $regex = "/\.({$acceptedExtensions})$/isU";

        foreach ($this->zip->listFiles("/^{$this->uploadedName}\//isU") as $fileName) {
            if (!preg_match($regex, $fileName)) {
                throw new BannerValidationException('Contain denied files');
            }
        }

        return $this;
    }

    protected function assetsLoadedOverHttpsOrRelativeAssert()
    {
        $content = $this->zip->getFileContent("$this->uploadedName/index.html");

        if (preg_match('/(src|href)=[\'"](\/|http:)/isU', $content)) {
            throw new BannerValidationException('All assets must load over https or relative');
        }

        return $this;
    }
}
