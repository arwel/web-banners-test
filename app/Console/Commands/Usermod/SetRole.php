<?php

namespace App\Console\Commands\Usermod;

use App\User;
use Illuminate\Console\Command;
use PHPZen\LaravelRbac\Model\Role;

class SetRole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'usermod:set-role {email} {--only} {--reset}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set role for user';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = User::where('email', $this->argument('email'))->firstOrFail();

        if ($this->option('reset')) {
            $this->resetUserRoles($user);
            $this->info('All done');

            return;
        }

        $roles = Role::all('slug')->pluck('slug')->toArray();

        $chosenSlug = $this->choice('Select role', $roles);
        $role = Role::where('slug', $chosenSlug)->firstOrFail();

        if ($this->option('only')) {
            $user->roles()->sync([$role->id]);
        } else {
            $user->roles()->attach($role->id);
        }

        $this->info('All done');
    }

    protected function resetUserRoles(User $user)
    {
        $slug = config('app.default_user_role', 'user');
        $role = Role::where('slug', $slug)->firstOrFail();

        $user->roles()->sync([$role->id]);
    }
}
