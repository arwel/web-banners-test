<?php

namespace App\Abstractions\Eloquent;

trait HasPaginationTrait
{
    public function scopeGetPaginated($query)
    {
        return $query->paginate(config('app.items_per_page', 10));
    }
}
