<?php

namespace App\Abstractions\Http;

use Chumper\Zipper\Facades\Zipper;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

trait CanUploadFileTrait
{
    public function upload(UploadedFile $file, Filesystem $disk): string
    {
        $datePath = $this->getDatePath();
        $this->mkdir($disk, $datePath);

        $name = $this->getUniqueName($disk, $datePath, $file->getClientOriginalName());

        return $disk->putFileAs($datePath, $file, $name);
    }

    public function uploadDirectory(UploadedFile $file, Filesystem $disk): string
    {
        $uploadedZip = $this->upload($file, $disk);
        $relativePathToZip = dirname($uploadedZip);
        $baseName = pathinfo($uploadedZip, PATHINFO_FILENAME);
        $uniqueName = $this->getUniqueName($disk, $relativePathToZip, $baseName);
        $destPath = $relativePathToZip . DIRECTORY_SEPARATOR . $uniqueName;

        $this->mkdir($disk, $destPath);

        Zipper::make($disk->path($uploadedZip))
        ->folder($baseName)
        ->extractTo($disk->path($destPath));

        $disk->delete($uploadedZip);

        return $destPath;
    }

    public function getUploadedDirectory(Request $request, string $fileKey, string $diskKey): string
    {
        if (!$request->hasFile($fileKey)) {
            return '';
        }

        return $this->uploadDirectory($request->file($fileKey), Storage::disk($diskKey));
    }

    public function getUploadedFileUrl(Request $request, string $fileKey, string $diskKey): string
    {
        if (!$request->hasFile($fileKey)) {
            return '';
        }

        $disk = Storage::disk($diskKey);

        return $disk->url($this->upload($request->file($fileKey), $disk));
    }

    protected function getDatePath(): string
    {
        return date('Y'.DIRECTORY_SEPARATOR.'m'.DIRECTORY_SEPARATOR.'j');
    }

    protected function getUniqueName(Filesystem $disk, string $path, string $name): string
    {
        while ($disk->exists($path . DIRECTORY_SEPARATOR . $name)) {
            $name = time() . '.' . $name;
        }

        return $name;
    }

    protected function mkdir(Filesystem $disk, string $name)
    {
        if (!$disk->exists($name)) {
            $disk->makeDirectory($name);
        }
    }
}
