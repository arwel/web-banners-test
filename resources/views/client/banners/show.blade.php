@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    {{ $item->title }}
                </div>

                <div class="card-body">
                    @if($item->preview)
                    <div>
                        <img src="{{ $item->preview }}" width="150">
                    </div>
                    @endif

                    <p class="mt-3 mb-3">
                        {{ $item->desc }}
                    </p>

                    @if($is_in_collection)
                    <div class="alert alert-info">
                        <b>For embed use URL:</b>&nbsp;<em>{{ route('web.client.banners.embed', ['token' => $collection->token]) }}</em>
                    </div>
                    @endif

                </div>

                <div class="card-footer">
                    <form class="form-inline" method="POST" action="{{ route('web.client.collections.store', ['banner' => $item]) }}">
                        @csrf
                        @method('POST')

                        <button class="btn btn-primary" type="submit">Add To Collection</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection