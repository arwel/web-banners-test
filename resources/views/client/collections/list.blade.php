@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Favorite Banners
                </div>

                <div class="card-body">

                    @if($items->isEmpty())
                    <center>Nothing found</center>
                    @else
                    <table class="table table-striped">
                        <thead class="thead-dark">
                            <tr>
                                <th>Preview</th>
                                <th>Title</th>
                                <th>Actions</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($items as $item)
                            <tr>
                                <td>
                                    @if(!empty($item->preview))
                                        <img src="{{ $item->preview }}" width="100">
                                    @else
                                        No
                                    @endif
                                </td>
                                <td>{{ $item->title }}</td>
                                <td>
                                    <a href="{{ route('web.client.banners.show', ['banner' => $item->slug]) }}" class="btn btn-default">Show</a><br>

                                    <form class="form-inline" method="POST" action="{{ route('web.client.collections.delete', ['banner' => $item->banner_id]) }}">
                                        @csrf
                                        @method('DELETE')

                                        <button class="btn btn-danger" type="submit">Delete</button>
                                    </form>

                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @endif


                </div>

                @if($items->total() > 1)
                <div class="card-footer">
                    {{ $items->links() }}
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection