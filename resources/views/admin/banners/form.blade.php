@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Banner [
                    <a href="{{ route('web.admin.banners.list') }}">List</a>
                    ]
                </div>

                <form class="card-body" method="POST" action="{{ $form_action }}" enctype="multipart/form-data">
                    @csrf
                    @method($form_method)

                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" name="title" id="title" value="{{ $item->title }}" class="form-control" required autofocus>
                    </div>

                    @if($item->id)
                    <div class="form-group">
                        <label for="slug">Slug</label>
                        <input type="text" id="slug" value="{{ $item->slug }}" class="form-control" readonly>
                    </div>
                    @endif

                    <div class="form-group">
                        <label for="desc">Description</label>
                        <textarea name="desc" id="desc" class="form-control" required>{{ $item->desc }}</textarea>
                    </div>

                    <div class="form-group">
                        @if($item->preview)
                        <img src="{{ $item->preview }}" width="150">
                        <br>
                        @endif
                        <label for="preview">Preview</label>
                        <input type="file" name="preview" id="preview"  class="form-control" >
                    </div>

                    <div class="form-group">
                        @if($item->archive)
                        {{ $item->archive }}
                        @endif
                        <label for="archive">Archive</label>
                        <input type="file" name="archive" id="archive"  class="form-control" >
                    </div>


                    <div class="form-group">
                        <button type="submit" class="btn btn-success">
                            {{ $item->id ? 'Update' : 'Create' }}
                        </button>
                    </div>
                </form>

                @if($item->id)

                <div class="card-footer">
                    <form action="{{ route('web.admin.banners.delete', ['banner' => $item,]) }}" method="POST" class="d-inline-block">
                        @csrf
                        @method('DELETE')

                        <button class="btn btn-danger">
                            Delete Permanently
                        </button>
                    </form>

                    <form action="{{ route('web.admin.banners.trash', ['banner' => $item,]) }}" method="POST" class="d-inline-block">
                        @csrf
                        @method('DELETE')

                        <button class="btn btn-danger">
                            Trash
                        </button>
                    </form>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection