@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Banners [
                    <a href="{{ route('web.admin.banners.list',) }}">Active</a> |
                    <a href="{{ route('web.admin.banners.list', ['type' => 'trashed']) }}">Trashed</a> |
                    <a href="{{ route('web.admin.banners.create') }}">Create</a>
                    ]
                </div>

                <div class="card-body">

                    @if($items->isEmpty())
                    <center>Nothing found</center>
                    @else
                    <table class="table table-striped">
                        <thead class="thead-dark">
                            <tr>
                                <th>ID</th>
                                <th>Preview</th>
                                <th>Title</th>
                                <th>Date</th>
                                <th>Actions</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($items as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>
                                    @if(!empty($item->preview))
                                        <img src="{{ $item->preview }}" width="100">
                                    @else
                                        No
                                    @endif
                                </td>
                                <td>{{ $item->title }}</td>
                                <td>{{ $item->created_at }}</td>
                                <td>
                                    @if(!$item->trashed())
                                    <a href="{{ route('web.admin.banners.edit', ['banner' => $item]) }}" class="btn btn-default">Edit</a><br>
                                    <form class="form-inline" method="POST" action="{{ route('web.admin.banners.trash', ['banner' => $item]) }}">
                                        @csrf
                                        @method('DELETE')

                                        <button class="btn btn-danger" type="submit">Trash</button>
                                    </form>
                                    @else
                                    <form class="form-inline" method="POST" action="{{ route('web.admin.banners.delete', ['banner' => $item]) }}">
                                        @csrf
                                        @method('DELETE')

                                        <button class="btn btn-danger" type="submit">Delete Permanently</button>
                                    </form>
                                    <br>
                                    <form class="form-inline" method="POST" action="{{ route('web.admin.banners.restore', ['banner' => $item]) }}">
                                        @csrf
                                        @method('PATCH')

                                        <button class="btn btn-info" type="submit">Restore</button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @endif


                </div>

                @if($items->total() > 1)
                <div class="card-footer">
                    {{ $items->links() }}
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection