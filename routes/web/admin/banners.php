<?php

Route::name('banners.')->group(function () {
    Route::group([
        'name' => 'banners.',
        'prefix' => 'banners',
    ], function () {
        Route::get('/', 'BannerController@index')->name('list');
        Route::get('/new', 'BannerController@create')->name('create');
        Route::post('/new', 'BannerController@store')->name('store');
        Route::get('/{banner}/edit', 'BannerController@edit')->name('edit');
        Route::patch('/{banner}/edit', 'BannerController@update')->name('update');
        Route::patch('/{banner}/restore', 'BannerController@restore')->name('restore');
        Route::delete('/{banner}/trash/clear', 'BannerController@clearTrash')->name('trash-clear');
        Route::delete('/{banner}/trash', 'BannerController@trash')->name('trash');
        Route::delete('/{banner}/delete', 'BannerController@delete')->name('delete');
    });
});
