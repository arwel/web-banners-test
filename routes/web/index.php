<?php

Route::name('admin.')->group(function () {
    Route::group([
        'namespace' => 'Admin',
        'middleware' => ['auth', 'rbac:is,admin',],
        'prefix' => 'admin',
    ], function () {
        require_once base_path('routes/web/admin/banners.php');

        Route::get('/', function () {
            return redirect()->route(config('app.default_admin_route'));
        })->name('dashboard');
    });
});

Route::name('client.')->group(function () {
    Route::group([
        'namespace' => 'Client',

    ], function () {
        require_once base_path('routes/web/client/banners.php');
        require_once base_path('routes/web/client/collections.php');

        Route::get('/', function () {
            return redirect()->route(config('app.default_client_route'));
        })->name('home');
    });
});

