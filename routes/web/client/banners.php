<?php

Route::name('banners.')->group(function () {
    Route::group([
        'prefix' => 'banners',
    ], function () {
        Route::get('/', 'BannerController@index')->name('list');
        Route::get('/banner/{slug}', 'BannerController@show')->name('show');
        Route::get('/embed/{token}/{path?}', 'BannerController@embed')->name('embed')->where([
            'path' => '.*(?!\.\.).*$',
        ]);
    });
});
