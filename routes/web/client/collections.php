<?php

Route::name('collections.')->group(function () {
    Route::group([
        'prefix' => 'collections',
        'middleware' => ['auth', 'rbac:is,user',],
    ], function () {
        Route::get('/', 'CollectionController@index')->name('list');
        Route::post('/{banner}/add', 'CollectionController@store')->name('store');
        Route::delete('/{banner}/delete', 'CollectionController@delete')->name('delete');
    });
});
