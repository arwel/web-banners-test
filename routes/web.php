<?php

Route::name('web.')->group(function () {
    require_once base_path('routes/web/index.php');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
