<?php

use Illuminate\Database\Seeder;
use PHPZen\LaravelRbac\Model\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            'Admin',
            'User',
            'Blocked',
        ];

        foreach ($roles as $title) {
            Role::firstOrCreate([
                'name' => $title,
                'slug' => Slugify::slugify($title),
                'description' => $title,
            ]);
        }
    }
}
